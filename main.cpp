#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <ctype.h>
#include <cmath>
#include <stack>

int calculate_energy(int, int);

int main(int argc, char* argv[]) {
    if (argc < 4) {
        std::cout << "Invalid number of parameters, please refer to the README to see how to run the program.\n";
        std::cout << "Terminating program.\n";
    }
    else {
        // Read the file contents and store in a buffer
        std::ifstream in_file;
        std::string in_file_name = argv[1];

        in_file.open(in_file_name.c_str(), std::ios::binary);
        
        struct stat file_status;

        stat(in_file_name.c_str(), &file_status);

        long file_size = file_status.st_size;
        char file_contents[file_size];

        in_file.read(file_contents, file_size);

        // Pointer to the start of the file
        char* file_ptr = file_contents;

        // Get the size of the image from the file
        // TODO: Is there a more efficient way to do this?
        int img_x = 0;
        int img_y = 0;
        bool x_filled = false;
        int mult = 1;
        while (*file_ptr != '\0') {
            if (isalpha(*file_ptr) || *file_ptr == '#') {
                // Skip to the next line
                while (*file_ptr != '\n') {
                    file_ptr++;
                }
                file_ptr++;
            } 
            else {
                if (*file_ptr == ' ') {
                    x_filled = true;
                    mult = 1;
                    file_ptr++;
                }
                if (*file_ptr == '\n') {
                    break;
                }
                if (x_filled) {
                    img_y = (img_y * mult) + (*file_ptr - '0');
                    mult *= 10;
                }
                else {
                    img_x = (img_x * mult) + (*file_ptr - '0');
                    mult *= 10;
                }
                file_ptr++;
            }
        }

        // Get the max value for any value of the image matrix
        file_ptr++; // Move on from the \n char

        int temp = 0;
        mult = 1;

        while (*file_ptr != '\n') {
            temp = (temp * mult) + (*file_ptr - '0');     
            if (mult < 10) { mult *= 10; }
            file_ptr++;
        }

        const int MAX_IMG_VALUE = temp;

        // Build the image matrix
        int img_matrix[img_y][img_x];

        // Initialize the matrix to all 0 to avoid issues with random
        // initializtion
        for (int i = 0; i < img_y; ++i) {
            for (int j = 0; j < img_x; ++j) {
                img_matrix[i][j] = 0;
            }
        }

                mult = 1;
        for (int i = 0; i < img_y; ++i) {
            for (int j = 0; j < img_x; ++j) {
                while (*file_ptr != ' ') {
                    img_matrix[i][j] = (img_matrix[i][j] * mult) + (*(file_ptr) - '0');
                    if (mult == 1) { mult *= 10; }
                    file_ptr++;
                }
                if (img_matrix[i][j] > MAX_IMG_VALUE) {
                    std::cout << "Something went wrong, a value is larger than it should be.\n";
                }
                file_ptr++;
            }
        }

        // TODO: Potentially start looping here for the number of
        // seams to be removed
        // Build the energy matrix
        int energy_matrix[img_y][img_x];

        int delta_x = 0;
        int delta_y = 0;

        for(int i = 0; i < img_y; ++i) {
            for (int j = 0; j < img_x; ++j) {
                // Change in x
                // Left edge case
                if (j == 0) {
                    delta_x = abs(img_matrix[i][j] - img_matrix[i][j+1]);
                }
                // Right edge case
                else if (j == img_x - 1) {
                    delta_x = abs(img_matrix[i][j] - img_matrix[i][j-1]);
                }
                else {
                    delta_x = abs(img_matrix[i][j] - img_matrix[i][j+1]) + abs(img_matrix[i][j] - img_matrix[i][j-1]);
                }

                // Change in y
                // Top edge case
                if (i == 0) {
                    delta_y = abs(img_matrix[i][j] - img_matrix[i+1][j]);
                }
                // Bottom edge case
                else if (i == img_y - 1) {
                    delta_y = abs(img_matrix[i][j] - img_matrix[i-1][j]);
                }
                else {
                    delta_y = abs(img_matrix[i][j] - img_matrix[i+1][j]) + abs(img_matrix[i][j] - img_matrix[i-1][j]);
                }

                energy_matrix[i][j] = delta_x + delta_y;
            }
        }

        // Build the cumulative energy matrix
        int cumulative_energy_matrix[img_y][img_x];

        // Top row of values is the same as the energy matrix
        for (int i = 0; i < img_x; ++i) {
            cumulative_energy_matrix[0][i] = energy_matrix[0][i];
        }

        // Fill the rest of the matrix by picking the minimum energy
        // of those available to the cell
        for (int i = 1; i < img_y; ++i) {
            for (int j = 0; j < img_x; ++j) {
                int min_energy_index;
                if (j == 0) {
                    if (energy_matrix[i-1][j] <= energy_matrix[i-1][j+1]) {
                        min_energy_index = j;
                    }
                    else {
                        min_energy_index = j+1;
                    }
                }
                else if (j > 0) {
                    if (energy_matrix[i-1][j-1] <= energy_matrix[i-1][j]) {
                        min_energy_index = j-1; 
                    }
                    else {
                        min_energy_index = j;
                    }
                    if (energy_matrix[i-1][min_energy_index] > energy_matrix[i-1][j+1]) {
                        min_energy_index = j+1;
                    }
                }
                else if (j == img_x-1) {
                    if (energy_matrix[i-1][j-1] <= energy_matrix[i-1][j]) {
                        min_energy_index = j-1;
                    }
                    else {
                        min_energy_index = j;
                    }
                }
                cumulative_energy_matrix[i][j] = energy_matrix[i][j] + energy_matrix[i-1][min_energy_index]; // + min of choices
            }
        }

        // Determine which vertical seams we are going to remove
        std::stack<int> remove;
        for (int i = img_y-1; i >= 0; ++i) {
            int min_index = 0;
            for (int j = img_x-1; j >= 0; ++j) {
                if (cumulative_energy_matrix[i][j] < cumulative_energy_matrix[i][min_index]) {
                    min_index = j;
                }
            }
            remove.push(min_index);
        }
    }

    return 0;
}
